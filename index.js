
let http = require("http");
let port = 3000;
let name = `Johnro`;


http.createServer(function(request, response) {
	if(request.url === '/') {
		response.write(`Welcome to Mobile Legends`);
		response.end();
	}
	else if(request.url === `/login`) {
		response.write(`Five seconds to enter the battle, ${name}`);
		response.end();
	}
	else if(request.url === `/register`) {
		response.write(`Create a hero that will fight for you!`);
		response.end();
	}
	else if(request.url === `/profile`) {
		response.write(`You are in profile page`);
		response.end();
	}
	else {
		response.write(`Location was not in Mobile Legends`);
		response.end();
	}
}).listen(port);


console.log(`Server is running successfully on port: ${port}
	▀██▀─▄███▄─▀██─██▀██▀▀█
	─██─███─███─██─██─██▄█
	─██─▀██▄██▀─▀█▄█▀─██▀█
	▄██▄▄█▀▀▀─────▀──▄██▄▄█
`);

